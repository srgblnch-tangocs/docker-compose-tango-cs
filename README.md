# [docker](https://www.docker.com/) [compose](https://docs.docker.com/compose/) for [TANGO Control System](https://www.tango-controls.org/)

![4 - Beta](https://img.shields.io/badge/Development_Status-3_--_alpha-yellow.svg)

## Prepare

After the clone, submodules with the container members is necessary 

```bash
git submodule init
git submodule update
```

With docker compose installed, then:

```bash
docker-compose build
```

## Testing

And let's start!

```bash
docker-compose up
```

After that, in your local machine, you can see the port 10000 exposed (not 
the 3306 from mariadb) and when you define the ```TANGO_HOST``` environment 
variable like ```export TANGO_HOST=localhost:10000``` then you will have access
to this service to launch _tango device servers_ and open an _ipython_ 
console and create proxies to those devices or use the standard tools for 
tango like _jive_ or _astor_.

## Production

(TODO): use ```docker swarm``` for more production like environment.